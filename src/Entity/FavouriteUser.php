<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FavouriteUserRepository")
 */
class FavouriteUser
{
    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private $followerId;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private $favUserId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     */
    private $favUsername;

    /**
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return integer
     */
    public function getFollowerId(): int
    {
        return $this->followerId;
    }

    /**
     * @param integer $followerId
     *
     * @return FavouriteUser
     */
    public function setFollowerId(int $followerId): FavouriteUser
    {
        $this->followerId = $followerId;

        return $this;
    }

    /**
     * @return integer
     */
    public function getFavUserId(): int
    {
        return $this->favUserId;
    }

    /**
     * @param integer $favUserId
     *
     * @return FavouriteUser
     */
    public function setFavUserId(int $favUserId): FavouriteUser
    {
        $this->favUserId = $favUserId;

        return $this;
    }

    /**
     * @return string
     */
    public function getFavUsername(): string
    {
        return $this->favUsername;
    }

    /**
     * @param string $favUsername
     *
     * @return FavouriteUser
     */
    public function setFavUsername(string $favUsername): FavouriteUser
    {
        $this->favUsername = $favUsername;

        return $this;
    }
}
