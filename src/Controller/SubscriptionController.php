<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 2019-02-02
 * Time: 22:48
 */

namespace App\Controller;

use App\Entity\SubscriptionEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SubscriptionController
 *
 * @package App\Controller
 * @Route("/sub")
 */
class SubscriptionController extends Controller
{
    /**
     * @Route("/follow")
     * @param Request                $request
     * @param EntityManagerInterface $em
     *
     * @return Response
     * @throws \Exception
     */
    public function followAction(Request $request, EntityManagerInterface $em)
    {
        if ($request->getMethod() === Request::METHOD_GET) {
            return new Response($request->query->get('hub_challenge'), Response::HTTP_ACCEPTED);
        }

        $data = json_decode($request->getContent(), true)['data'];

        // @todo need to track event by id to avoid duplication of event get from Twitch
        foreach ($data as $row) {
            /** @var SubscriptionEvent $record */
            $record = (new SubscriptionEvent())
                ->setUserId($row['from_id'])
                ->setEvent('user ' . $row['from_name'] . ' start follow user ' . $row['to_name'])
                ->setRawResponse((string) $request)
                ->setDateAdded((new \DateTime()));

            $em->persist($record);
        }
        $em->flush();

        return new Response('', Response::HTTP_OK);
    }

    /**
     * @Route("/stream")
     * @param Request                $request
     * @param EntityManagerInterface $em
     *
     * @return Response
     * @throws \Exception
     */
    public function streamAction(Request $request, EntityManagerInterface $em)
    {
        if ($request->getMethod() === Request::METHOD_GET) {
            return new Response($request->query->get('hub_challenge'), Response::HTTP_ACCEPTED);
        }

        $data = json_decode($request->getContent(), true)['data'];

        // @todo need to track event by id to avoid duplication of event get from Twitch
        foreach ($data as $row) {
            /** @var SubscriptionEvent $record */
            $record = (new SubscriptionEvent())
                ->setUserId($row['user_id'])
                ->setEvent('user ' . $row['user_name'] . ' start stream: ' . $row['title'])
                ->setRawResponse((string) $request)
                ->setDateAdded((new \DateTime()));

            $em->persist($record);
        }
        $em->flush();

        return new Response('', Response::HTTP_OK);
    }
}