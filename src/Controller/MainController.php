<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 10/02/18
 * Time: 00:44
 */

namespace App\Controller;

use App\Component\TwitchProvider;
use App\Component\UserSubscriber;
use App\Repository\FavouriteUserRepository;
use App\Repository\SubscriptionEventRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class MainController extends Controller
{
    /**
     * @Method("GET")
     * @Route("/", name="index_page")
     * @Template("index.html.twig")
     *
     * @param Request                 $request
     * @param Session                 $session
     * @param TwitchProvider          $provider
     *
     * @param FavouriteUserRepository $favUserRepo
     *
     * @return array|RedirectResponse
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \League\OAuth2\Client\Provider\Exception\IdentityProviderException
     */
    public function indexAction(
        Request $request,
        Session $session,
        TwitchProvider $provider,
        FavouriteUserRepository $favUserRepo
    ) {
        $code  = $request->get('code');
        $token = $session->get('access_token'); // @todo 'access_token' string better to move in constant of separate service
        if (!$token && !$code) {
            return ['authUri' => $provider->getAuthUri()];
        }

        if ($code) {
            $token = $provider->getAccessToken($code);
            $session->set('access_token', \json_encode($token));
            // @todo implementing full security system using sec. bundle not necessary (for a while) for such simple app
            // @todo so just write in session and execute this simple checks

            return $this->redirectToRoute('index_page');
        }

        if ($token) {
            $owner   = $provider->getOwner($token);
            $favUser = $favUserRepo->getFavUser($owner->getId());
            $resp    = ['username' => $owner->getUsername()];
            if ($favUser) {
                $resp['favUsername'] = $favUser->getFavUsername();
                $resp['favUserId']   = $favUser->getFavUserId();
            }
            return $resp;
        }
    }

    /**
     * @Method("POST")
     * @Route("/add-fav-user/{username}", name="add_fav_user")
     *
     * @param string         $username
     * @param TwitchProvider $provider
     * @param UserSubscriber $userSubscriber
     *
     * @return JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addFavUserAction(string $username, TwitchProvider $provider, UserSubscriber $userSubscriber)
    {
        $resp = [];
        try {
            $favUser = $provider->getUserIdByName($username);
            $userSubscriber->subscribe($favUser);
        } catch (\Exception $e) {
            $resp = ['error' => 'Something went wrong'];
        }

        return new JsonResponse($resp);
    }

    /**
     * @Method("POST")
     * @Route("/remove-fav-user/{favUserId}", name="remove_fav_user")
     *
     * @param string         $favUserId
     * @param TwitchProvider $provider
     * @param UserSubscriber $userSubscriber
     *
     * @return JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function removeFavUserAction(string $favUserId, TwitchProvider $provider, UserSubscriber $userSubscriber)
    {
        $resp        = [];
        $currentUser = $provider->getCurrentUser();
        if (!$currentUser || !$favUserId) {
            return $this->redirectToRoute('index_page');
        }
        try {
            $userSubscriber->unsubscribe($favUserId);
        } catch (\Exception $e) {
            $resp = ['error' => 'Something went wrong'];
        }

        return new JsonResponse($resp);
    }

    /**
     * @Method("GET")
     * @Route("/streamer", name="page_streamer")
     * @Template("streamer.html.twig")
     *
     * @param FavouriteUserRepository     $favUserRepo
     * @param SubscriptionEventRepository $subEvtRepo
     * @param TwitchProvider              $provider
     *
     * @return array|RedirectResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function streamerAction(
        FavouriteUserRepository $favUserRepo,
        SubscriptionEventRepository $subEvtRepo,
        TwitchProvider $provider
    ) {
        $currentUser = $provider->getCurrentUser();
        // @todo need to rewrite this code to combine check with te second wone
        if (!$currentUser) {
            return $this->redirectToRoute('index_page');
        }

        $favUser = $favUserRepo->getFavUser($currentUser->getId());
        if (!$favUser) {
            return $this->redirectToRoute('index_page');
        }

        return [
            'topEvents'   => $subEvtRepo->findLastEvents($favUser->getFavUserId()),
            'favUsername' => $favUser->getFavUsername(),
        ];
    }
}