<?php

namespace App\Repository;

use App\Entity\SubscriptionEvent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SubscriptionEvent|null find($id, $lockMode = null, $lockVersion = null)
 * @method SubscriptionEvent|null findOneBy(array $criteria, array $orderBy = null)
 * @method SubscriptionEvent[]    findAll()
 * @method SubscriptionEvent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubscriptionEventRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SubscriptionEvent::class);
    }

    /**
     * @param int $userId
     * @param int $numOfLastEvts
     *
     * @return array|ArrayCollection
     */
    public function findLastEvents(int $userId, int $numOfLastEvts = 10)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.userId = :user')
            ->setParameter('user', $userId)
            ->orderBy('s.id', 'DESC')
            ->setMaxResults($numOfLastEvts)
            ->getQuery()
            ->getResult();
    }
}
