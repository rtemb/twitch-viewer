<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 29/07/2018
 * Time: 21:52
 */

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MaintControllerTest extends WebTestCase
{
    /**
     * @group functional
     */
    public function testIndexAction()
    {
        $client = static::createClient();

        $client->request(Request::METHOD_GET, '/');
        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

    }
}