<?php
/**
 * Created by PhpStorm.
 * User: abarhanov
 * Date: 1/31/19
 * Time: 1:36 PM
 */

namespace App\Component;

use Depotwarehouse\OAuth2\Client\Twitch\Entity\TwitchUser;
use Depotwarehouse\OAuth2\Client\Twitch\Provider\Exception\TwitchIdentityProviderException;
use Depotwarehouse\OAuth2\Client\Twitch\Provider\Twitch;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Token\AccessTokenInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class TwitchProvider    //@todo ideally we hve to to split this class because not it looks like God object
{
    private const ID_HOST  = 'https://id.twitch.tv'; // @todo Better to move to env variable
    private const API_HOST = 'https://api.twitch.tv';//

    private const URI_TOKEN         = '/oauth2/token';
    private const URI_SUBSCRIPTIONS = '/helix/webhooks/hub';

    private const SUB_USER_FOLLOW   = '/sub/follow';
    private const SUB_STREAM_CHANGE = '/sub/stream';


    /**  @var Twitch */
    private $client;

    /**  @var string */
    private $clientId;

    /**  @var string */
    private $clientSecret;

    /**  @var string */
    private $callbackHost;

    /**  @var Session */
    private $session;

    private const DEFAULT_SCOPE = ['user_read', 'channel_read'];

    public function __construct(string $clientId, string $clientSecret, string $callbackHost, Session $session)
    {
        $this->clientId     = $clientId;
        $this->clientSecret = $clientSecret;
        $this->session      = $session;
        $this->callbackHost = $callbackHost;

        $this->client = new Twitch([
            'clientId'     => $clientId,
            'clientSecret' => $clientSecret,
            'redirectUri'  => $callbackHost,
        ]);
    }

    /**
     * @return Twitch
     */
    public function getClient(): Twitch
    {
        return $this->client;
    }

    /**
     * @return string
     */
    private function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @return string
     */
    private function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    /**
     * @return Session
     */
    private function getSession(): Session
    {
        return $this->session;
    }

    /**
     * @return string
     */
    public function getCallbackHost(): string
    {
        return $this->callbackHost;
    }

    /**
     * @return string
     */
    public function getAuthUri(): string
    {
        return $this->getClient()->getAuthorizationUrl(['scope' => self::DEFAULT_SCOPE]);
    }

    /**
     * @param string $code
     *
     * @return AccessTokenInterface
     * @throws IdentityProviderException
     */
    public function getAccessToken(string $code): AccessTokenInterface
    {
        return $this->getClient()->getAccessToken(
            "authorization_code", [
            'code' => $code,
        ]);
    }

    /**
     * @return TwitchUser|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCurrentUser()
    {
        $token = $this->getSession()->get('access_token');

        return $token ? $this->getOwner($token) : null;
    }

    /**
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function isAuthorized(): bool
    {
        return (bool) $this->getCurrentUser();
    }

    /**
     * @param string $token
     *
     * @return TwitchUser
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getOwner(string $token): TwitchUser
    {
        $accessToken = new AccessToken(json_decode($token, true));

        try {
            /** @var TwitchUser $owner */
            $owner = $this->getClient()->getResourceOwner($accessToken);
        } catch (TwitchIdentityProviderException $e) {
            /** @var AccessToken $refreshedToken */
            $refreshedToken = $this->refreshToken($accessToken);

            $owner = $this->getClient()->getResourceOwner($refreshedToken);
        }

        return $owner;
    }


    /**
     * @param string $username
     *
     * @return TwitchUser
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function getUserIdByName(string $username): TwitchUser
    {
        $resp = $this->getClient()->getHttpClient()->request(
            Request::METHOD_GET,
            'https://api.twitch.tv/kraken/users?login=' . $username, [
                'headers' => [
                    'Accept'    => 'application/vnd.twitchtv.v5+json',
                    'Client-ID' => $this->getClientId(),
                ],
            ]
        );

        $users = \json_decode($resp->getBody(), true);
        if (!$users['_total']) {
            throw new \Exception('User ' . $username . ' not found');
        }
        $user              = $users['users'][0];
        $user['email']     = ''; // @todo create this keys just to use existing TwitchUser class
        $user['partnered'] = '';

        return new TwitchUser($user);
    }


    /**
     * @param AccessToken $accessToken
     *
     * @return AccessTokenInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function refreshToken(AccessToken $accessToken)
    {
        $uri = self::ID_HOST . self::URI_TOKEN . '?' . http_build_query([
                'grant_type'    => 'refresh_token',
                'refresh_token' => $accessToken->getRefreshToken(),
                'client_id'     => $this->getClientId(),
                'client_secret' => $this->getClientSecret(),

            ]);

        $resp = $this->getClient()->getHttpClient()->request(Request::METHOD_POST, $uri);
        // @todo Better to add checks for http errors

        $this->getSession()->set('access_token', (string) $resp->getBody());

        return new AccessToken(\json_decode($resp->getBody(), true));
    }

    /**
     * @param string $userId
     * @param string $action
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function followWebHook(string $userId, $action = 'subscribe'): void
    {
        $url = self::API_HOST . self::URI_SUBSCRIPTIONS;
        $req = new \GuzzleHttp\Psr7\Request(
            Request::METHOD_POST,
            $url,
            [
                'Accept'       => 'application/vnd.twitchtv.v5+json',
                'Client-ID'    => $this->getClientId(),
                'Content-Type' => 'application/json',
            ], json_encode([
            'hub.mode'          => $action,
            'hub.lease_seconds' => 864000,  // @todo need to track this time to renew subscription
            'hub.callback'      => $this->getCallbackHost() . self::SUB_USER_FOLLOW,
            'hub.topic'         => 'https://api.twitch.tv/helix/users/follows?first=1&from_id=' . $userId,
            // @todo topic uri better to move in constant
        ]));

        $resp = $this->getClient()->getHttpClient()->send($req);

        if ($resp->getStatusCode() !== Response::HTTP_ACCEPTED) {
            throw new \Exception();
        }
    }

    /**
     * @param string $userId
     * @param string $action
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function streamWebHook(string $userId, $action = 'subscribe'): void
    {
        $url = self::API_HOST . self::URI_SUBSCRIPTIONS;
        $req = new \GuzzleHttp\Psr7\Request(
            Request::METHOD_POST,
            $url,
            [
                'Accept'       => 'application/vnd.twitchtv.v5+json',
                'Client-ID'    => $this->getClientId(),
                'Content-Type' => 'application/json',
            ], json_encode([
            'hub.mode'          => $action,
            'hub.lease_seconds' => 864000,  // @todo need to track this time to renew subscription
            'hub.callback'      => $this->getCallbackHost() . self::SUB_STREAM_CHANGE,
            'hub.topic'         => 'https://api.twitch.tv/helix/streams?user_id=' . $userId,
            // @todo topic uri better to move in constant
        ]));

        $resp = $this->getClient()->getHttpClient()->send($req);

        if ($resp->getStatusCode() !== Response::HTTP_ACCEPTED) {
            throw new \Exception();
        }
    }
}
