<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SubscriptionEventRepository")
 */
class SubscriptionEvent
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $event;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $dateAdded;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=false)
     */
    private $rawResponse;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     *
     * @return SubscriptionEvent
     */
    public function setUserId(int $userId): SubscriptionEvent
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getEvent(): string
    {
        return $this->event;
    }

    /**
     * @param string $event
     *
     * @return SubscriptionEvent
     */
    public function setEvent(string $event): SubscriptionEvent
    {
        $this->event = $event;
        return $this;
    }

    /**
     * @return string
     */
    public function getRawResponse(): string
    {
        return $this->rawResponse;
    }

    /**
     * @param string $rawResponse
     *
     * @return SubscriptionEvent
     */
    public function setRawResponse(string $rawResponse): SubscriptionEvent
    {
        $this->rawResponse = $rawResponse;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateAdded(): \DateTime
    {
        return $this->dateAdded;
    }

    /**
     * @param \DateTime $dateAdded
     *
     * @return SubscriptionEvent
     */
    public function setDateAdded(\DateTime $dateAdded): SubscriptionEvent
    {
        $this->dateAdded = $dateAdded;
        
        return $this;
    }
}
