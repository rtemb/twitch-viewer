FavUser = {
    save: function (e) {
        const uri = e.dataset.uri;
        const username = document.getElementById('fav-username').value;
        const errorBlock = document.getElementById('fav-user-error');
        const subscribeBlock = document.getElementById('subscribe-block');
        const subSuccess = document.getElementById('subscribe-success');

        if (!username) {
            errorBlock.innerHTML = 'favourite username shoud not be blank';
            errorBlock.style.display = 'inline';
        }

        fetch(uri + username, {
            method: "POST"
        })
            .then(function (res) {
                subscribeBlock.style.display = 'none';
                subSuccess.style.display = 'block';
            })
            .catch(function (res) {
                errorBlock.innerHTML = 'Something went wrong';
                errorBlock.style.display = 'inline';
            });
    },
    remove: function (e) {
        const uri = e.dataset.uri;
        const favUserId = e.dataset.id;
        const errorBlock = document.getElementById('remove-fav-user-error');

        fetch(uri + favUserId, {
            method: "POST"
        })
            .then(function (res) {
                window.location.reload();
            })
            .catch(function (res) {
                errorBlock.innerHTML = 'Something went wrong';
                errorBlock.style.display = 'inline';
            });
    }
};