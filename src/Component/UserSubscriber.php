<?php
/**
 * Created by PhpStorm.
 * User: artyomb
 * Date: 2019-02-02
 * Time: 22:31
 */

namespace App\Component;

use App\Entity\FavouriteUser;
use App\Repository\FavouriteUserRepository;
use App\Repository\SubscriptionEventRepository;
use Depotwarehouse\OAuth2\Client\Twitch\Entity\TwitchUser;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Exception\GuzzleException;

class UserSubscriber
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var TwitchProvider */
    private $twitchProvider;

    /** @var FavouriteUserRepository */
    private $favouriteUserRepository;

    /** @var SubscriptionEventRepository */
    private $subEvtRepo;

    /**
     * UserSubscriber constructor.
     *
     * @param EntityManagerInterface      $em
     * @param TwitchProvider              $twitchProvider
     * @param FavouriteUserRepository     $favouriteUserRepository
     * @param SubscriptionEventRepository $subEvtRepo
     */
    public function __construct(
        EntityManagerInterface $em,
        TwitchProvider $twitchProvider,
        FavouriteUserRepository $favouriteUserRepository,
        SubscriptionEventRepository $subEvtRepo
    ) {
        $this->em                      = $em;
        $this->twitchProvider          = $twitchProvider;
        $this->favouriteUserRepository = $favouriteUserRepository;
        $this->subEvtRepo              = $subEvtRepo;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEm(): EntityManagerInterface
    {
        return $this->em;
    }

    /**
     * @return TwitchProvider
     */
    public function getTwitchProvider(): TwitchProvider
    {
        return $this->twitchProvider;
    }

    /**
     * @return FavouriteUserRepository
     */
    public function getFavouriteUserRepository(): FavouriteUserRepository
    {
        return $this->favouriteUserRepository;
    }

    /**
     * @return SubscriptionEventRepository
     */
    public function getSubEvtRepo(): SubscriptionEventRepository
    {
        return $this->subEvtRepo;
    }

    /**
     * @param TwitchUser $favUser
     *
     * @throws GuzzleException
     */
    public function subscribe(TwitchUser $favUser)
    {
        $follower = $this->getTwitchProvider()->getCurrentUser();

        if (!$this->getFavouriteUserRepository()->isSomeoneSubscribed($favUser->getId())) {
            $this->getTwitchProvider()->followWebHook($favUser->getId());
            $this->getTwitchProvider()->streamWebHook($favUser->getId());
        }

        $exist = $this->getFavouriteUserRepository()->getFavUser($follower->getId());
        if (!$exist) {
            $record = (new FavouriteUser())
                ->setFollowerId((int) $follower->getId())
                ->setFavUserId((int) $favUser->getId())
                ->setFavUsername($favUser->getUsername());

            $this->getEm()->persist($record);
            $this->getEm()->flush();
        }
    }

    /**
     * @param string $favUserId
     *
     * @throws GuzzleException
     * @throws \Doctrine\DBAL\DBALException
     */
    public function unsubscribe(string $favUserId)
    {
        $follower = $this->getTwitchProvider()->getCurrentUser();
        $this->getEm()->getConnection()->executeQuery(
            'DELETE FROM favourite_user WHERE follower_id = :id', ['id' => $follower->getId()]
        );

        if (!$this->getFavouriteUserRepository()->isSomeoneSubscribed($favUserId)) {
            $this->getTwitchProvider()
                ->followWebHook($favUserId, 'unsubscribe'); // @todo unsubscribe better to move in constant
            $this->getTwitchProvider()
                ->streamWebHook($favUserId, 'unsubscribe');
        }
    }
}