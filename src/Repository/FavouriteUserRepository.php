<?php

namespace App\Repository;

use App\Entity\FavouriteUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method FavouriteUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method FavouriteUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method FavouriteUser[]    findAll()
 * @method FavouriteUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FavouriteUserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FavouriteUser::class);
    }

    /**
     * @param int $id
     *
     * @return FavouriteUser|null
     */
    public function getFavUser(int $id): ?FavouriteUser
    {
        return $this->findOneBy(['followerId' => $id]);
    }

    /**
     * @param int      $userId
     * @param int|null $exceptUser
     *
     * @return bool
     */
    public function isSomeoneSubscribed(int $userId, int $exceptUser = null)
    {
        $qb = $this->createQueryBuilder('f')
            ->andWhere('f.favUserId = :userId')
            ->setParameter('userId', $userId);

        if ($exceptUser) {
            $qb->andWhere('f.followerId != :except')
                ->setParameter('except', $exceptUser);
        }

        return (bool) $qb->setMaxResults(1)->getQuery()->getResult();
    }
}
